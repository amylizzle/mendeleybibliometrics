Go to https://mendeley-show-me-access-tokens.herokuapp.com/
Log in, copy access token, run
java MenedelyBibliometrics <ACCESSTOKEN>


It'll run, and you'll need to keep an eye on it (to refresh mendeley token and get around scholar's bot protections). When it's done, it'll create "output.csv" with all your papers, their folders, their year, their scholar citations and their scopus citations.