import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.reflocator.cites4.GoogleScholar;
import org.reflocator.cites4.MemoryTable;
import org.reflocator.cites4.Record;
import org.json.*;
import org.w3c.dom.Document;

public class MendeleyBibliometrics {
	static String accessToken = "";
	
	public static void main(String[] args)
	{
		try{
			
			if(args.length > 0)
				accessToken = args[0];
			else
			{
				System.out.print("Please enter your Mendeley API access token: ");
				Scanner inAPI = new Scanner(System.in);
				accessToken = inAPI.nextLine();
				//inAPI.close();
			}
			
			//request folder list
			String foldersJSON = MendeleyAPIRequest("https://api.mendeley.com/folders");
			//System.out.println(foldersJSON);
			//parse folder list, query user for selection
			JSONObject folderstmp = new JSONObject("{folders:"+foldersJSON+"}");
			JSONArray folderArray = folderstmp.getJSONArray("folders");
			System.out.println("Please choose a folder (subfolders will be included):");
			for(int i = 0; i<folderArray.length(); i++)
			{
				System.out.println(i+") "+folderArray.getJSONObject(i).getString("name"));
			}
			System.out.print("Folder number: ");
			Scanner in = new Scanner(System.in);
			int n = 0;
			while (in.hasNext()){
			    if (in.hasNextInt()){
			        n = in.nextInt();
			        break;
			    } else {
			        in.next(); // Just discard this, not interested...
			    }
			}
			
			//in.close();
			String folderUID = folderArray.getJSONObject(n).getString("id");
			List<String> foldersToScan = new ArrayList<String>();
			List<String> folderFriendlyNames = new ArrayList<String>();
			foldersToScan.add(folderUID);
			folderFriendlyNames.add(folderArray.getJSONObject(n).getString("name"));
			for(int i = 0; i<folderArray.length(); i++)
			{
				if(!folderArray.getJSONObject(i).isNull("parent_id"))
					if(folderArray.getJSONObject(i).getString("parent_id").equalsIgnoreCase(folderUID))
					{
						foldersToScan.add(folderArray.getJSONObject(i).getString("id"));
						folderFriendlyNames.add(folderArray.getJSONObject(i).getString("name"));
					}
			}

			List<String> csvOutput = new ArrayList<String>();
			csvOutput.add("\"Paper Title\", \"Folder Name\", \"Year\", \"Google Cites\", \"Scopus Cites\"");
			System.out.println(csvOutput.get(0));
			
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			GoogleScholar gs = new GoogleScholar();
			Random r = new Random();
			boolean giveUpOnGoogle = false;
			for(int j=0; j<foldersToScan.size(); j++)
			{
				folderUID = foldersToScan.get(j);

				//read list of papers from folder

				String papersJSON = "{papers:"+MendeleyAPIRequest("https://api.mendeley.com/folders/"+folderUID+"/documents")+"}";
				//System.out.println(papersJSON);
				//for each document, query DOI, then WoS for AMR & IC
				//System.out.println("Resolving DOIs...");
				JSONObject paperstmp = new JSONObject(papersJSON);
				JSONArray papersArray = paperstmp.getJSONArray("papers");
				System.out.println("Got a total of "+papersArray.length()+" papers from folder "+folderFriendlyNames.get(j));
				
				
				for(int i = 0; i<papersArray.length(); i++)
				{
					String csvLine = "";
					String documentJSON = MendeleyAPIRequest("https://api.mendeley.com/documents/"+papersArray.getJSONObject(i).getString("id"));
					JSONObject document = new JSONObject(documentJSON); 

					csvLine = "\""+document.getString("title")+"\",\""+folderFriendlyNames.get(j)+"\"";

					if(!giveUpOnGoogle)
					{
						try{
							MemoryTable records = (MemoryTable) gs.getRecordsByUrl("https://scholar.google.com/scholar?start=0&num=100&hl=en&as_sdt=1,5&q="+document.getString("title"));
							if (records.getSize() > 0)
							{
								Record paperRecord = records.getRecordAt(0);
								//System.out.println("Google Cites:"+paperRecord.getValue("cites"));
								csvLine = csvLine+",\""+paperRecord.getValue("year")+"\",\""+paperRecord.getValue("cites")+"\"";
								
							}
							else
							{
								System.out.println("Couldn't find this paper on Google Scholar! "+document.getString("title"));
								csvLine = csvLine+","+"\"???\",\"???\"";
							}
							
							//Stop google from getting mad at us - random wait between 10 and 50 seconds
							Thread.sleep(30000+(r.nextInt(40000)-20000));

						}
						catch(Exception e)
						{
							if(!e.getClass().getName().equalsIgnoreCase("HttpStatusException"))
								e.printStackTrace();
							else
								System.out.println("ERROR: Looks like a bad cookie :(");
							//giveUpOnGoogle = true;
							csvLine = csvLine+",\"???\",\"???\"";
						}
					}
					else
					{
						csvLine=csvLine+",\"???\",\"???\"";
					}
					//
					//"pmid":"25246403","issn":"1098-6596","isbn":"978-3-540-89186-4","arxiv":"arXiv:1011.1669v3","doi":"10.1007/978-3-540-89187-1"
					//


					if(	!document.isNull("identifiers"))
					{
						JSONObject identifiers = document.getJSONObject("identifiers");
						boolean gotInfo = false;
						if(!gotInfo){
							try{
								//System.out.println(document.getJSONObject("identifiers").getString("doi"));
								//get citation count + other identifiers by doi or pmid (possibly more)
								//https://api.elsevier.com/content/search/scopus?query=DOI(10.1007/978-3-540-89187-1)&apiKey=0fb143e5387e7ae488f2ba37d63a20b2
								DocumentBuilder db = dbf.newDocumentBuilder();
								URL url = new URL("https://api.elsevier.com/content/search/scopus?query=DOI("+document.getJSONObject("identifiers").getString("doi")+")&apiKey=0fb143e5387e7ae488f2ba37d63a20b2");
								HttpURLConnection connection = (HttpURLConnection) url.openConnection();
								connection.setRequestMethod("GET");
								connection.setRequestProperty("Accept","application/xml");
								InputStream is = connection.getInputStream();

								Document dom = db.parse(is);
								is.close();
								if(dom.getElementsByTagName("citedby-count").getLength()>0)
								{
									csvLine = csvLine+",\""+dom.getElementsByTagName("citedby-count").item(0).getTextContent()+"\"";
									gotInfo = true;
								}
							}
							catch(Exception e)
							{
								//e.printStackTrace();
								//System.out.println("NO DOI!");
							}
						}
						if(!gotInfo){
							try{
								//System.out.println(identifiers.getString("pmid"));
								//get citation count + other identifiers by doi or pmid (possibly more)
								//https://api.elsevier.com/content/search/scopus?query=DOI(10.1007/978-3-540-89187-1)&apiKey=0fb143e5387e7ae488f2ba37d63a20b2
								DocumentBuilder db = dbf.newDocumentBuilder();
								URL url = new URL("https://api.elsevier.com/content/search/scopus?query=PMID("+identifiers.getString("pmid")+")&apiKey=0fb143e5387e7ae488f2ba37d63a20b2");
								HttpURLConnection connection = (HttpURLConnection) url.openConnection();
								connection.setRequestMethod("GET");
								connection.setRequestProperty("Accept","application/xml");
								InputStream is = connection.getInputStream();

								Document dom = db.parse(is);
								is.close();
								if(dom.getElementsByTagName("citedby-count").getLength()>0)
								{
									csvLine = csvLine+",\""+dom.getElementsByTagName("citedby-count").item(0).getTextContent()+"\"";
									gotInfo = true;
								}
							}
							catch(Exception e)
							{
								//e.printStackTrace();
								//System.out.println("NO PMID!");
							}
						}

						if(!gotInfo){
							try{
								//System.out.println(document.getJSONObject("identifiers").getString("isbn"));
								//get citation count + other identifiers by doi or pmid (possibly more)
								//https://api.elsevier.com/content/search/scopus?query=DOI(10.1007/978-3-540-89187-1)&apiKey=0fb143e5387e7ae488f2ba37d63a20b2
								DocumentBuilder db = dbf.newDocumentBuilder();
								URL url = new URL("https://api.elsevier.com/content/search/scopus?query=ISBN("+document.getJSONObject("identifiers").getString("isbn")+")&apiKey=0fb143e5387e7ae488f2ba37d63a20b2");
								HttpURLConnection connection = (HttpURLConnection) url.openConnection();
								connection.setRequestMethod("GET");
								connection.setRequestProperty("Accept","application/xml");
								InputStream is = connection.getInputStream();

								Document dom = db.parse(is);
								is.close();
								if(dom.getElementsByTagName("citedby-count").getLength()>0)
								{
									csvLine = csvLine+",\""+dom.getElementsByTagName("citedby-count").item(0).getTextContent()+"\"";
									gotInfo = true;
								}
							}
							catch(Exception e)
							{
								//e.printStackTrace();
								//System.out.println("NO DOI!");
							}
						}
						if(!gotInfo)
							csvLine=csvLine+",\"???\"";
					}
					else
						csvLine=csvLine+",\"???\"";

					csvOutput.add(csvLine);
					System.out.println("Done "+(i+1)+"/"+papersArray.length()+" papers of folder "+(j+1)+"/"+foldersToScan.size());
					//System.out.println(csvLine);
				}
				
			}
			//output results to csv
			FileWriter writer = new FileWriter("output.csv"); 
			for(String str: csvOutput) {
			  writer.write(str+"\r\n");
			}
			writer.close();
			System.out.println("Finished!");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static String MendeleyAPIRequest(String URL)
	{
		try{
			URL url = new URL(URL+"?limit=200");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Authorization" ,"Bearer "+accessToken);



			//Get Response  
			InputStream is = connection.getInputStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is));
			StringBuilder response = new StringBuilder(); // or StringBuffer if Java version 5+
			String line;
			while ((line = rd.readLine()) != null) {
				response.append(line);
				response.append('\r');
			}
			//rd.close();
			return response.toString();
		}
		catch(Exception e)
		{
			System.out.println("Your Mendeley API token didn't work. It may have expired.");
			System.out.print("Please enter your Mendeley API access token: ");
			Scanner inAPI = new Scanner(System.in);
			accessToken = inAPI.nextLine();
			//inAPI.close();
			return MendeleyAPIRequest(URL);
		}
	}
}
